var map = new BMap.Map("container");//这里是现在页面的哪一个id位置
var point = new BMap.Point(104.0664031000,30.5793108400);//这个是显示具体地址，纬度和经度
map.centerAndZoom(point, 20);//显示的距离
map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
var opts1 = {offset: new BMap.Size(100, 5)}
//var opts2 = { offset: new BMap.Size(260, 100) }
map.addControl(new BMap.NavigationControl());
map.addControl(new BMap.ScaleControl(opts1));
map.addControl(new BMap.GeolocationControl());
var marker = new BMap.Marker(point); // 创建标注
map.addOverlay(marker);    // 将标注添加到地图中
var companyName = {
    width: 80,     // 信息窗口宽度
    height: 30,     // 信息窗口高度
    title: "万息科技"  // 信息窗口标题
}
var infoWindow = new BMap.InfoWindow("万息科技集训岗前中心", companyName);  // 创建信息窗口对象
map.openInfoWindow(infoWindow, map.getCenter());      // 打开信息窗口